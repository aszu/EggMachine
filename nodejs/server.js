const net = require('net')
const express = require('express');


var JSONDuplexStream = require('json-duplex-stream');


const EggBroker = require('./eggbroker');


const tcpServer = net.createServer(
   function (conn) {
      var stream = JSONDuplexStream();

      console.info("CB1");
      var eggBroker = new EggBroker();
      var eggController = eggBroker.getController();
      eggController.setDeviceConnected(true);
      eggController.setDeviceStream(stream);

      conn.setTimeout(10000);

      conn.on('end', function(e, c) {
            console.info("Went away", e, c);
            eggController.setDeviceConnected(false);
      });
      conn.on('timeout', function(e,c) {
            console.info("Timeouting", e, c);
            conn.end();
            conn.destroy();
            eggController.setDeviceConnected(false);
      });
      conn.on('error', function(e,c) {
            console.info('SOCK ERROR');
      });
      conn.on('close', function(e,c) {
            console.info("SOCK CLOSE");
      });

      conn
         .pipe(stream.in)
         .pipe(eggBroker)
         .pipe(stream.out)
         .pipe(conn);

      stream.in.on('error', function(e) {
         console.info("JSON STREAM IN - ERROR:\n"+ e.stack);
         conn.end();
      } );


//      setInterval(function() {
//            console.info("PING!");
//            conn.write('\n');
//      }, 1000);

   }
);


const app = express();

app.use('/', express.static('../client'))

app.get('/DTH', function(req, res) {
      res.status(200);
      res.write("HI");
      res.end();
});


var io ;


setTimeout(function() {
      tcpServer.listen(9001);
      var server = app.listen(9002);
      io = require('socket.io').listen(server);

      io.on('connect', function(sock) {
            var eggBroker = new EggBroker();
            var eggController = eggBroker.getController();

            function emitStats() {
//               console.info('bcast: ', eggController.getClientStats());
               sock.emit('stats', eggController.getClientStats());
            }

            eggController.addClientListener(sock, function(data) {
                  //emitStats();
                  if (typeof data !== 'undefined') {
                       sock.emit('measurement', data);
                  } else {
                       emitStats();
                  }
            });

            console.info("Someone hits us");

            sock.on('targetChanged', function(targetSpecification) {
                 console.info("TargetChanged received", targetSpecification);
                 eggController.setTargetSpecification(targetSpecification);
            });
            sock.on('pwmChanged', function(pwmSpec) {
                  console.info("PWM changed", pwmSpec);
                  eggController.setPWMSpecification(pwmSpec);
            });
            sock.on('pidChanged', function(pidSpec) {
                  console.info("PID changed", pidSpec);
                  if (pidSpec.code != '1FX') {
                     sock.emit('alert', 'Wrong code');
                  } else {
                     eggController.setPIDSpecification(pidSpec.ratios);
                     sock.emit('pidChanged', eggController.getStatus().heatC.ratios);
                  }
            });
            sock.on('stats', function() {
                  console.info("Received something");
                  emitStats();
            });
            sock.on('disconnect', function() {
                  console.info("Disconnecting.");
                  eggController.delClientListener(sock);
            });
      });
});
