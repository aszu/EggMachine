var Transform = require('stream').Transform;
var util = require('util');

if (!Object.assign) {
   Object.assign = require('object-assign');
}


var EggBroker = function () {
   if (!EggBroker.prototype.eggController) {
      EggBroker.prototype.eggController = new EggBrokerController();
   }
   console.info("broker constructed");
   Transform.call(this, {objectMode: true});

};

util.inherits(EggBroker, Transform);

EggBroker.prototype._transform = function(chunk, encoding, done) {
   //console.info("EGGBROKER TRANSFORM", arguments);
   var result;

   result = this.eggController.deviceMessage(chunk);
   if (!result) {
      result  = this.eggController.getStatus();
   }
   //console.info(result);
   var me = this;
   me.push(result || {ooops: 'nothing returned?'});
   done();
}


EggBroker.prototype.getController = function() {
   return this.eggController;
}

/*************** Egg Controller methods ***************/

var EggBrokerController = function () {
   this.clientListeners = [];
   this.temperatureHistory = [];
   this.PWM = {clock:0, duty:0};
   this.heatC = {ratios:{}};
};

EggBrokerController.prototype.deviceMessage = function(command) {
   this.commandsProcessed = this.commandsProcessed + 1 || 0;
   if (!this.bConnected) {
      this.setDeviceConnected(true);
   }

   if (command.cleanRequest) {
      this.temperatureHistory.length = 0;
      this.notifyClientListeners();
   }



   var controller = this;
   if (command.heatC) {
      Object.keys(command.heatC).forEach(function(key) {
            if (key != 'ratios') {
               controller.heatC[key] = command.heatC[key];
            }
      });

      Object.keys(command.heatC.ratios).forEach(function(key) {
            if (typeof(controller.heatC.ratios[key] === 'undefined')) {
               controller.heatC.ratios[key] = command.heatC.ratios[key];
            }
      });

   }

   if (typeof command.temperature != 'undefined') {
      this.temperatureHistory.push({output: command.output, time:this.commandsProcessed, temperature: command.temperature, sensors: command.sensors});
      this.notifyClientListeners({PID:this.PID, output: command.output, time:this.commandsProcessed, temperature: command.temperature, sensors: command.sensors, heat:command.heat, heatC: command.heatC});
   }

   return null
}

EggBrokerController.prototype.getStatus = function() {
   return {
      devConnected: this.bConnected,
      target: this.targetSpecification || {temperature: 20},
      commProcessed: this.commandsProcessed,
      pwm: this.PWM,
      heatC: this.heatC
   };
}

EggBrokerController.prototype.getTempHistory = function() {
   var result = this.temperatureHistory;

   return result;
}

EggBrokerController.prototype.setDeviceConnected = function(bConnected) {
   console.info("Set dev connected: ", bConnected);
   this.bConnected = bConnected;
   this.notifyClientListeners();
}

EggBrokerController.prototype.addClientListener = function (sock, callback) {
   this.clientListeners.push({sock: sock, callback: callback});
}

EggBrokerController.prototype.delClientListener = function(sock, callback) {
   for (var i = this.clientListeners.length-1; i>=0; i--) {
      console.info("Deleting listener", i);
      if (sock === this.clientListeners[i].sock) {
         this.clientListeners.splice(i, 1);
      };
   }
}

EggBrokerController.prototype.notifyClientListeners = function (data) {
   for (var i = 0; i<this.clientListeners.length; i++) {
      this.clientListeners[i].callback.call(this,data);
   }
}

EggBrokerController.prototype.getClientStats = function () {
   return  {statData: this.getStatus(), tempHistory: this.getTempHistory()};
}

EggBrokerController.prototype.setTargetSpecification = function(targetData) {
   var me = this;
   this.clientListeners.forEach(function(listener) {
         me.targetSpecification = targetData;
         listener.sock.emit('targetChanged', targetData);
   });
}

EggBrokerController.prototype.setPWMSpecification = function(pwm) {
   var me = this;
   this.PWM = pwm;
   this.clientListeners.forEach(function(listener) {
         listener.sock.emit('pwmChanged', pwm);
   })
};

EggBrokerController.prototype.setPIDSpecification = function(pid) {

   console.info(">>>>>",pid);
   this.deviceStream.out.write({EVENT: 'PID_PARAM', ratios:pid});

   Object.assign(this.heatC.ratios, pid);
   this.clientListeners.forEach(function(listener) {
         listener.sock.emit('pidChanged', pid)
   })
}

EggBrokerController.prototype.setDeviceStream = function(stream) {
   this.deviceStream = stream;
}

module.exports  = EggBroker;