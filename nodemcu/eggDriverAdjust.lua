
heat = {
   status = 0, -- 0 - not heating, 1 - heating, 2 - stabilization
   timer  = 0,
   applied = 0,  -- applied heat units (outputLevel * seconds)
   startTemp = 0, -- temperature at which power was applied
   lastTemp = 0,  -- temperature in previous
   delta = 0,      -- degrees per heat unit (last measured)
   overshoot = 0,
   undershoot = 0,
   lastCorrection = 0,
   calibrator = 2
}

function adjustOutput(temp)

   if ( math.abs(temp - targetSetting) < math.abs(heatC.ratios.CONTINUOUS)) then
      return adjustOutputContinuous(temp)
   else
      heatC.initialized = 0
   end

   -- calibration block. checking
   if (heat.status == 0) then
      if (heat.delta < 0) then
         heat.delta = 0
      end

      if (heat.delta == 0) then
         outputLevel = heat.calibrator
         heat.calibrator = heat.calibrator + 1
         heat.startTemp = temp
         heat.timer = 5
         heat.applied = outputLevel * heat.timer
         heat.status = 1
      else
         local jump = targetSetting - temp

         if (jump < 0) then
            jump = 0
         end

         if (heat.undershoot < 1) then
            heat.lastCorrection = heat.lastCorrection + heat.undershoot
         else
            if (heat.overshoot > 0) then
               heat.lastCorrection = heat.lastCorrection - (heat.overshoot - 0.1) / 2
            end
         end
         --  TODO: Enable overshoot correction by removing following line
         heat.lastCorrection = 0


         local estimatedUnits = (jump + heat.lastCorrection) / heat.delta
         local expectedTime = math.ceil(jump)*5
         if (expectedTime > 30) then
            expectedTime = 30
         else
            if (expectedTime < 4) then
               expectedTime = 4
            end
         end
         local expectedOutput = estimatedUnits / expectedTime
         if (expectedOutput > 20) then
            expectedOutput = 20
         end
         if (expectedOutput < 0) then
            expectedOutput = 0
         end
         if (expectedOutput > 2 and expectedOutput < 10 and expectedTime < 7) then
            expectedOutput = expectedOutput / 2
            expectedTime = expectedTime * 2
         end
         --if (expectedOutput > 0 and expectedOutput < 2 and expectedTime > 5) then
         --   expectedOutput = expectedOutput * 2
         --   expectedTime = math.ceil(expectedTime / 2)
         --
         --   if (expectedOutput * expectedTime > estimatedUnits) then
         --      expectedOutput = estimatedUnits / expectedTime
         --   end
         --end

         -- TODO: add overshoot / undershoot code here

         heat.applied = expectedOutput * expectedTime

         heat.timer = expectedTime
         heat.startTemp = temp
         heat.status = 1

         outputLevel = expectedOutput

      end
   else
      if (heat.status == 1) then
         heat.timer = heat.timer - 1
         if (heat.timer == 0) then
            heat.status = 2
            heat.timer = 10
            outputLevel = 0
         end
      else
         if (heat.status == 2) then

            if (temp == heat.lastTemp) then
               heat.timer = heat.timer - 1
            else
               if (temp < heat.lastTemp) then
                  heat.timer = heat.timer - 10
               else
                  heat.timer = heat.timer - 0.1
               end
            end

            if (heat.timer <= 0) then
               heat.status = 0
               if (heat.applied ~= 0) then
                  local newDelta = (temp-heat.startTemp) / heat.applied
                  if (newDelta < heat.delta) then
                     heat.delta = heat.delta * 0.95 + newDelta * 0.05
                  else
                     heat.delta = newDelta
                  end
               end

               if (temp < targetSetting) then
                  heat.undershoot = targetSetting - temp
                  heat.overshoot = 0
               else
                  heat.overshoot = temp-targetSetting
                  heat.undershoot = 0
               end
            end
         end
      end
   end


   heat.lastTemp = temp

   -- end in end, we have to got power in between 0 and 20, where 0 is no power and 20 is 'full ahead'.
   if (outputLevel > 20) then
      outputLevel = 20
   else
      if (outputLevel < 0) then
         outputLevel = 0
      end
   end

   -- at the end, we have one more safety switch - if temp is higher than target, force "full stop".
   if (targetSetting < temp) then
      outputLevel = 0
   end



   --print("OUTPUT: ", outputLevel)
end