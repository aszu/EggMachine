function dynamicWifiConnect()
   wifi.setmode(wifi.STATIONAP)
   wifi.sta.getap( function (names)
      local name
      for name in pairs(names) do
         print(name)
      	 name = name:gsub(' ', '_')
         local result, existence = pcall(function() return file.exists('wifiauto_'..name..'.lua') end);
         if result and existence then
            print('Autoconnecting wifi: '..name)
            require('wifiauto_'..name)
            break
         end
      end
   end)
end


function initAction()


   -- connect to wifi, prepare to set current time on connection made.
   wifi.sta.eventMonReg(wifi.STA_GOTIP,
      function (prevState)
         rtctime = require('rtctime')
         http.get('http://aszu.pl:8181/.../services/now/1000', nil,
         function(s,b)
            if tonumber(b) == nil or tonumber(b) < 10 then
               print('Cannot feed RTC with "'..b..'"')
               node.restart()
            else
               rtctime.set(tonumber(b))
               print('Starting...')
               pcall(startApplication)
            end
         end)
      end
   )

   wifi.sta.eventMonStart()

   --wifi.sta.connect()
   pcall(dynamicWifiConnect())

   -- this setups the output for the heater...
   gpio.mode(5,gpio.OUTPUT);
   gpio.write(5,gpio.LOW);

end


function startApplication()
   require('eggDriver')
   startEggDriver()
end

tmr.alarm(6,10000,tmr.ALARM_SINGLE,
function ()
   print('Initialization...')
   pcall(initAction)
end
);