print ('eggdriver loaded ')
require('eggDriverAdjust')
require('eggDriverContinuous')


isConnected = 0
outputLevel = 0
targetSetting = 20
pwmClock = 0
pwmDuty = 0
cleanRequest = 0


function startEggDriver()
   print('eggDriver started at '..rtctime.get())
   connectToCommandServer()
   ds = require('ds18b20');
   ds.setup(6);
   gpio.mode(4,gpio.OUTPUT)
   gpio.write(4, gpio.HIGH)
end

function connectToCommandServer()
   connection = net.createConnection(net.TCP, 0)
   connection:on('connection', function()
      print('CONN')
      isConnected = 2
   end)
   connection:on('reconnection', function()
      print('RECONN')
   end)
   connection:on('disconnection', function()
      print('DISCONN')
      isConnected = 0
   end)
   connection:on('receive', function(sck, dataRcv)
      local struct = cjson.decode(dataRcv)
      local k, v

      if(struct.EVENT ~= nil) then
         print ("EVENT: "..struct.EVENT);
         if (struct.EVENT == 'PID_PARAM') then
            for k,v in pairs(struct.ratios) do
               heatC.ratios[k] = v
            end
         end
         return
      end

      targetSetting = struct.target.temperature
      if (struct.pwm ~= nil) then
         if (struct.pwm.duty ~= pwmDuty or struct.pwm.clock ~= pwmClock) then
            pwmDuty = struct.pwm.duty;
            pwmClock = struct.pwm.clock;
            if (pwmClock ~= 0 and pwmDuty ~= 0) then
               pwm.setup(7,pwmClock,pwmDuty)
               pwm.start(7)
            else
               pwm.close(7)
            end
         end
      end
      -- what to do on nodejs to nodemcu communication?

   end)
   connection:on('sent', function()
    --  print('SENT')
   end)

   connection:connect(9001, 'aszu.pl');

   tmr.alarm(6,1000,tmr.ALARM_AUTO, function()
      if (isConnected == 0) then
         connectToCommandServer()
         isConnected = 1;
      end
      if (isConnected == 2) then
         reportTemperature()
      end
   end);


   gpio.mode(4, gpio.OPENDRAIN)
   gpio.write(4, gpio.HIGH)

   tmr.alarm(5,500, tmr.ALARM_AUTO, function()
        if (outputLevel > 0) then
           tmr.register(4,  math.ceil( outputLevel*24 ) , tmr.ALARM_SINGLE, function()
           -- print("ShortTimerStop")
           gpio.write(4, gpio.HIGH)
         end);
         gpio.write(4, gpio.LOW)
         if (not tmr.start(4)) then
            print("Unable to start disable timer")
            gpio.write(4, gpio.HIGH)
         end
        end
   end);
end


function reportTemperature()
  addrs = ds.addrs()
  temp = 0

  if (#addrs == 0) then
   print("OOPS - no sensors")
   return
  end

  sensors = {}
  count = 0

  for i, val in pairs(addrs) do
    vals = crypto.toHex(val);
   temptemp = ds.read(val);
   if (temptemp ~= nil) then
      temp = temp + temptemp
      sensors[vals] = temptemp
      count = count + 1
   end
  end


  if (count ~= 0) then
   temp = temp / count
  else
   temp = 85
  end

  if (temp ~= 85) then
   adjustOutput(temp)
   connection:send(cjson.encode({temperature=temp, cleanRequest = cleanRequest, sensors=sensors, output=outputLevel, heat=heat, heatC = heatC}).."\n")
   cleanRequest = 0
  end
end



gpio.mode(3, gpio.INT)
gpio.trig(3, "down", function() print("Clean request"); cleanRequest=1 end)
pwm.setup(7,1,0)
