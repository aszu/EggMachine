heatC = {
   ratio = 0,
   d1 = 0,
   d5 = 0,
   i  = 0,
   i40 = 0,
   p40 = 0,
   p20 = 0,
   p = 0,
   lastTemp = 0,
   lastDiff = 0,
   initialized = 0,
   ratios = {
      d1  = 100,
      d5  = 60,
      i   = 0.1,
      i40 = 0.4,
      p20  = 5,
      p   = 1,
      CUTOUT  = -0.5,
      CONTINUOUS = 1
   }
}

function adjustOutputContinuous(temp)
   if (heatC.initialized ~= 1) then
      heatC.lastTemp = temp
      heatC.initialized = 1
      return
   end

   local diff = (targetSetting - temp)

   heatC.d1 = diff - heatC.lastDiff
   heatC.d5 = heatC.d5 * 0.8 + heatC.d1 * 0.2

   heatC.i = heatC.i + diff
   heatC.p40 = heatC.p40 * 0.975 + diff * 0.025
   heatC.i40 = 40 * heatC.p40

   heatC.p20 = heatC.p20 * 0.8 +  diff * 0.2

   heatC.p = diff

   local ratios = heatC.ratios
   outputLevel = ratios.d1 * heatC.d1 + ratios.d5 * heatC.d5 + ratios.i * heatC.i + ratios.i40 * heatC.i40 + ratios.p20 * heatC.p20 + ratios.p * heatC.p

   heatC.lastDiff = diff
   heatC.lastTemp = temp

   if (-diff > math.abs(ratios.CUTOUT)) then
      outputLevel = 0
   end


end
