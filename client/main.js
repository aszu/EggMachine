var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}
// If the page is hidden, pause the video;
// if the page is shown, play the video
function handleVisibilityChange() {
  if (document[hidden]) {
    socket.close();
  } else {
    socket.open();
  }
}

document.addEventListener(visibilityChange, handleVisibilityChange, false);


var socket = io.connect();
socket.on('alert', function (a) { alert(a) } );
socket.on('stats', applyStats);
socket.on('measurement', addMeasurement);
socket.on('connect', function() {
      socket.emit('stats');
});
socket.on('targetChanged', function (targetData) {
     console.info("Callback target received", targetData);
     if (targetData.temperature != parseFloat(document.getElementById('eggDriverTargetTemperature').value)) {
        document.getElementById('eggDriverTargetTemperature').value = targetData.temperature;
     }
});
socket.on('pwmChanged', function(pwmData) {
   document.getElementById('pwmClock').value = pwmData.clock;
   document.getElementById('pwmDuty').value = pwmData.duty;
});
socket.on('pidChanged', function(pidData) {
      updateHeatC({ratios: pidData})
})

var targetParameters = {};

var targetWatchInterval = setInterval(
   function () {
      var temperature= parseFloat(document.getElementById('eggDriverTargetTemperature').value)
      if (typeof temperature != 'undefined' && !Number.isNaN(temperature) && temperature != null && temperature != targetParameters.temperature) {
         targetParameters.temperature = temperature;
         eggDriverTargetTemperatureChangedDebounced();
      }
   }, 100
);

var pwmChangedDebounced = _.debounce(pwmChanged, 500);
var pidChangeDebounced = _.debounce(pidChange, 500);
var callPidChangeDebounced = function () {
   pidChangeDebounced._PENDING_ = true;
   pidChangeDebounced.apply(this,arguments);
}
function pidChange() {
   var ratios = {};
   d3.select('#tweakTableParamRow').selectAll('input').nodes().forEach(function(input) {
         ratios[input.dataset.parameter] = parseFloat(input.value) || 0;
   });
   console.info(ratios)
   socket.emit('pidChanged', {code: document.getElementById('tweakCode').value || '' , ratios:ratios})
   pidChangeDebounced._PENDING_ = false;
}

var eggDriverTargetTemperatureChangedDebounced = _.debounce(eggDriverTargetTemperatureChanged, 1000);


function pwmChanged() {
   socket.emit('pwmChanged', {duty: parseInt(document.getElementById('pwmDuty').value),clock: parseInt(document.getElementById('pwmClock').value)});
}

function eggDriverTargetTemperatureChanged() {
   socket.emit('targetChanged', {temperature: parseFloat(document.getElementById('eggDriverTargetTemperature').value)});
   drawTempChartDebounced(undefined,true);
}

var cachedHistoryData = [];

window.addEventListener('resize', resize);

function resize() {
   drawTempChartDebounced(undefined,true);
}


function applyStats(pIn) {
   var statData, tempHistory;
   statData = pIn.statData;
   tempHistory = pIn.tempHistory;
   var h1 = d3.select('#connIndicator').data([statData])
      .attr('class', function (d) { return d.devConnected?'device-connected-indicator':'device-disconnected-indicator'} )

        console.info(pIn);


   document.getElementById('eggDriverTargetTemperature').value = pIn.statData.target.temperature;
   document.getElementById('pwmDuty').value=pIn.statData.pwm.duty;
   document.getElementById('pwmClock').value =pIn.statData.pwm.clock;
   updateHeatC(pIn.statData.heatC);


   cachedHistoryData = tempHistory;
   drawTempChartDebounced(tempHistory);
}


function updateHeatC(heatC) {

   if (pidChangeDebounced._PENDING_) {
      return
   }
   var keys = Object.keys(heatC.ratios).sort();
   var table = document.getElementById('tweakTable');

   if (!table.rows.length) {
      var heads = table.insertRow();
      var params = table.insertRow();
      params.setAttribute('id', 'tweakTableParamRow');

      var values = table.insertRow();
      keys.forEach(function(key) {
            var headCell = heads.insertCell();
            var paramCell = params.insertCell();
            var valueCell = values.insertCell();

            var type = ['tweakHead', 'tweakParam', 'tweakValue'];
            [headCell, paramCell, valueCell].forEach(function(cell) {
               cell.dataset.parameter = key;
               cell.className = type.shift();
            })

            var paramInput = paramCell.appendChild(document.createElement('input'));
            paramInput.addEventListener('input', callPidChangeDebounced);



            var valueInput = valueCell.appendChild(document.createElement('input'));
            valueInput.setAttribute('disabled','1');

            [paramInput, valueInput].forEach(function(input) {
                  input.setAttribute('type', 'number');
                  input.setAttribute('step','0.025');
                  input.setAttribute('min', -10000);
                  input.setAttribute('max',  10000);
                  input.dataset.parameter=key;
            });

            headCell.innerHTML = key;

      });

   }

   keys.forEach(function(key) {

         var paramInput = d3.select(table.rows[1]).select('input[data-parameter='+key+']').nodes()[0];
         var valueInput = d3.select(table.rows[2]).select('input[data-parameter='+key+']').nodes()[0];

         paramInput.value = heatC.ratios[key];
         valueInput.value = parseFloat(heatC[key]).toFixed(6) || '';

   });


   return [keys, table]
}


function test() {
   return updateHeatC({
         ratios: {
            p1: 22,
            p:  12,
            d:   1+Math.random(),
            d5: 12,
            i20: 1
         },
         p1: 22,
         p:  12,
         d:   1,
         d5: 12+Math.random(),
         lastTemp: 12
   })
}


function addMeasurement(data) {
   cachedHistoryData.push(data);
   document.getElementById('eggDriverCurrentTemperature').value = data.temperature.toFixed(2);
   var sensorRange = d3.extent(_.values(data.sensors));
   document.getElementById('eggDriverCurrentDelta').value = ( sensorRange[1] - sensorRange[0] ).toFixed(2);
   //console.info(JSON.stringify(data.heat));
   document.getElementById('eggDriverCurrentOutput').value = data.output || '0';

   if (data.heatC) {
      updateHeatC(data.heatC);
   };

   drawTempChartDebounced(cachedHistoryData);
}

var drawTempChartDebounced = _.debounce(drawTempChart,300);

function drawTempChart(tempHistory, bDoItFast) {
   var timeScale;
   var tempScale;

   var iAnimationDuration = bDoItFast?1:300;

   var svg = d3.select('#processChart');

   if (!tempHistory) {
      tempHistory = svg.selectAll('path').datum();
   }

   var width, height;
   var svgNode = svg.node();
   var svgBCR = svgNode.getBoundingClientRect()
   width = svgNode.clientWidth || (svgBCR.right - svgBCR.left);
   height = svgNode.clientHeight || (svgBCR.bottom - svgBCR.top)
   var margin = {top: 20, right: 50, bottom: 30, left: 50}

   var e = parseFloat(document.getElementById('chartRangeMaxInput').value);
   var rangeMax = 60*Math.pow(2, e);

   var tempLimit=targetParameters.temperature;


   var timeScaleExtent = d3.extent(tempHistory, function (d) { return d.time } );
   if (timeScaleExtent[1] - timeScaleExtent[0] < 20) {
      timeScaleExtent[0] = timeScaleExtent[1] - 20;
   } else if (timeScaleExtent[1] - timeScaleExtent[0] > rangeMax) {
      timeScaleExtent[0] = timeScaleExtent[1] - rangeMax;
   }
   timeScale = d3.scaleLinear()
      .domain(timeScaleExtent)
      .range([ margin.left , width - margin.right ])
      .clamp(true);

   var tempScaleExtent = d3.extent(d3.merge([d3.extent(tempHistory, function (d) { return d.temperature } ),[tempLimit]]));
   tempScaleExtent[0] -= 1;
   tempScaleExtent[1] += 1;
   tempScale = d3.scaleLinear()
      .domain(tempScaleExtent)
      .range([ height - margin.bottom,  margin.top ]);

   var line = d3.line()
      .x(function(d) { return timeScale(d.time)})
      .y(function(d) { return tempScale(d.temperature)})
      .defined(function(d) { return typeof d.temperature != 'undefined'} );

   var paths = svg.selectAll('path.chartDataLine').data([tempHistory]);

   paths = paths.enter().append('path').merge(paths)
      .attr('class', 'chartDataLine')
      .style('fill', 'none')
      .style('stroke', 'black')
      .style('stroke-width', '2px')
      .transition()
      .duration(iAnimationDuration)
      .attr('d', line);


   var knownSensors = {};
   tempHistory.forEach(function(elem) {
        for (var i in elem.sensors) {
          if (elem.sensors.hasOwnProperty(i)) {
             knownSensors[i] = 1;
          }
        }
   });
   knownSensors = Object.keys(knownSensors);

   var sensorPaths = svg.selectAll('path.sensorChartDataLine').data(knownSensors);
   sensorPaths = sensorPaths.enter().append('path').merge(sensorPaths)
       .attr('class', 'sensorChartDataLine')
     .style('fill', 'none')
     .style('stroke', 'orange')
     .style('stroke-width', '0.8px')
     .each(
      function(datum, index, nodes) {
         var result = [];

         var selfPath = d3.select(this);

         tempHistory.forEach(function (elem,index,array) {
            result.push({time: elem.time, temperature: elem.sensors[datum]});
         });

         selfPath.data([result]);
         return result;
      })
    .transition()
    .duration(iAnimationDuration)
     .attr('d', line);



   var leftAxis = svg.select('.leftAxis');
   if (leftAxis.size() < 1) {
      leftAxis = svg.append('g').attr('class', 'leftAxis')
         .attr('transform', 'translate('+margin.left+',0)');
   }
   leftAxis.transition().duration(iAnimationDuration).call(d3.axisLeft(tempScale).ticks(10));

   var leftAxisLines = svg.selectAll('line.leftAxisGuide').data(tempScale.ticks(10).reverse());
   leftAxisLines.exit().remove();

   leftAxisLines.enter().append('line')
     .attr('x1', function(d) { return margin.left})
        .attr('y1', function(d) { return tempScale(d)})
        .attr('x2', function(d) { return width - margin.right})
        .attr('y2', function(d) { return tempScale(d)})
        .style('opacity', '0')
        .merge(leftAxisLines)
        .attr('class', 'leftAxisGuide')
        .transition()
      .duration(iAnimationDuration)
        .style('opacity', '1')
        .attr('x1', function(d) { return margin.left})
        .attr('y1', function(d) { return tempScale(d)})
        .attr('x2', function(d) { return width - margin.right})
        .attr('y2', function(d) { return tempScale(d)})
     .style('stroke', 'gray')
      .style('stroke-width', '0.5px');

   var targetLines= svg.selectAll('line.targetLines').data([tempLimit]);
   targetLines.exit().remove();

   targetLines.enter().append('line')
      .attr('class', 'targetLines')
     .attr('x1', function(d) { return margin.left || 0})
        .attr('y1', function(d) { return tempScale(d) || 0})
        .attr('x2', function(d) { return (width - margin.right) || 0})
        .attr('y2', function(d) { return tempScale(d) || 0})
        .style('opacity', '0')
        .merge(targetLines)
      .transition()
      .duration(iAnimationDuration)
        .style('opacity', '1')
        .attr('x1', function(d) { return margin.left || 0})
        .attr('y1', function(d) { return tempScale(d) || 0})
        .attr('x2', function(d) { return (width - margin.right) || 0})
        .attr('y2', function(d) { return tempScale(d) || 0})
     .style('stroke', 'brown')
      .style('stroke-width', '0.9px');


   var downAxis = svg.select('.downAxis');
   if (downAxis.size() < 1) {
      downAxis = svg.append('g').attr('class', 'downAxis')
         .attr('transform', 'translate(0,'+(height-margin.bottom)+')');
   }
   downAxis.transition().duration(iAnimationDuration).call(d3.axisBottom(timeScale).ticks(width/70));



}
